//
//  dishwasherappTests.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 20/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import XCTest
@testable import dishwasherapp

class MockProductDataSource: ProductDataSource {
    
    var fetchDataCallCount = 0
    
    override func fetchData() {
        fetchDataCallCount += 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
}

class ProductListViewControllerTests: XCTestCase {
    
    var productListViewController: ProductListViewController!
    var mockProductDataSource: MockProductDataSource!
    var mockCollectionView: MockCollectionView!
    
    override func setUp() {
        super.setUp()
        
        mockProductDataSource = MockProductDataSource()
        mockCollectionView = MockCollectionView()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        productListViewController = storyboard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        productListViewController.dataSource = mockProductDataSource
    }
    
    override func tearDown() {
        productListViewController = nil
        mockProductDataSource = nil
        mockCollectionView = nil
        super.tearDown()
    }
    
    func testViewDidLoad_InitialisesDataSource() {
        
        // When
        
        productListViewController.appear()
        
        // Then
        
        XCTAssertTrue(productListViewController.dataSource.delegate === productListViewController)
        XCTAssertTrue(productListViewController.collectionView.dataSource === mockProductDataSource)
        XCTAssertEqual(mockProductDataSource.fetchDataCallCount, 1)
    }
    
    func testViewDidLoad_WhenVCIsFirstLoaded_LoadingStateIsShown() {
        
        // When
        
        productListViewController.appear()
        
        // Then
        
        XCTAssertTrue(productListViewController.spinner.isAnimating)
        XCTAssertTrue(productListViewController.collectionView.isHidden)
        XCTAssertEqual(mockProductDataSource.fetchDataCallCount, 1)
        XCTAssertEqual(productListViewController.title, "Dishwashers")
    }
    
    func testDidFetchData_WhenCalled_RemovesLoadingState() {
        
        // Given
        
        productListViewController.appear()
        
        // When
        
        productListViewController.didFetchData()
        
        // Then
        
        XCTAssertFalse(productListViewController.spinner.isAnimating)
        XCTAssertTrue(productListViewController.spinner.isHidden)
        XCTAssertFalse(productListViewController.collectionView.isHidden)
    }
    
    func testDidFetchData_WhenCalled_ReloadsCollectionView() {
        
        // Given
    
        productListViewController.appear()
        productListViewController.collectionView = mockCollectionView
        
        // When
        
        productListViewController.didFetchData()
        
        // Then
    
        XCTAssertTrue(mockCollectionView.reloadDataCount == 1)
    }

    func testDidFetchData_WhenCalled_UpdatesTitle() {
        
        // Given
        
        productListViewController.appear()
        
        // When
        
        productListViewController.didFetchData()

        // Then
        
        XCTAssertEqual(productListViewController.title, "Dishwashers (5)")
    }
    
    func testDidFailToFetchData_WhenCalled_RemovesLoadingState() {
        
        // Given
        
        productListViewController.appear()
        
        // When
        
        productListViewController.didFailToFetchData()
        
        // Then
        
        XCTAssertFalse(productListViewController.spinner.isAnimating)
        XCTAssertTrue(productListViewController.spinner.isHidden)
        XCTAssertTrue(productListViewController.collectionView.isHidden)
    }
    
}
