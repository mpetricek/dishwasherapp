//
//  dishwasherappTests.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 20/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import XCTest
@testable import dishwasherapp

class MockProductDataSourceDelegate: ProductDataSourceDelegate {

    var didFetchDataCallCount = 0
    func didFetchData() {
        didFetchDataCallCount += 1
    }
    
    var didFailToFetchDataCallCount = 0
    func didFailToFetchData() {
        didFailToFetchDataCallCount += 1
    }
}

class ProductDataSourceTests: XCTestCase {
    
    var sut: ProductDataSource!
    var mockURLSession: MockURLSession!
    var mockProductParser: MockProductParser!
    var mockProductDataSourceDelegate: MockProductDataSourceDelegate!
    var sampleProduct: Product!
    
    override func setUp() {
        super.setUp()
        sampleProduct = Product(id: 1, title: "a product 1", priceInPounds: 100, imageURL: URL(string: "http://example.com/image")!)
        mockURLSession = MockURLSession()
        mockProductParser = MockProductParser()
        mockProductDataSourceDelegate = MockProductDataSourceDelegate()
        sut = ProductDataSource(urlSession: mockURLSession, productParser: mockProductParser)
        sut.delegate = mockProductDataSourceDelegate
    }
    
    override func tearDown() {
        sampleProduct = nil
        sut = nil
        mockURLSession = nil
        mockProductParser = nil
        mockProductDataSourceDelegate = nil
        super.tearDown()
    }
    
    func testFetchData_WhenNetworkSucceeds_ParsesTheDataAndNotifiesTheDelegate() {
        
        // Given
        
        let expectedData = "some_json_data".data(using: String.Encoding.utf8)
        mockURLSession.dataTaskReturns = (expectedData, nil, nil)
        mockProductParser.parseReturns = [ sampleProduct ]
        
        // When
        
        sut.fetchData()
        
        // Then
        
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 2))
        
        XCTAssertEqual(mockProductParser.parseArgsForCall.last, expectedData)
        XCTAssertEqual(mockProductDataSourceDelegate.didFetchDataCallCount, 1)
    }
    
    func testFetchData_WhenNetworkFails_NotifiesTheDelegate() {
        
        // Given
        
        mockURLSession.dataTaskReturns = (nil, nil, NSError(domain: "fatal error", code: 666, userInfo: nil) )
        mockProductParser.parseReturns = [ sampleProduct ]
        
        // When
        
        sut.fetchData()
        
        // Then
        
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 2))
        
        XCTAssertNil(mockProductParser.parseArgsForCall.last)
        XCTAssertEqual(mockProductDataSourceDelegate.didFailToFetchDataCallCount, 1)
    }
    
    func testFetchData_WhenParserFails_NotifiesTheDelegate() {
        
        // Given
        
        let expectedData = "some_unparseable_data".data(using: String.Encoding.utf8)
        mockURLSession.dataTaskReturns = (expectedData, nil, nil)
        mockProductParser.parseReturns = nil
        
        // When
        
        sut.fetchData()
        
        // Then
        
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 2))
        XCTAssertEqual(mockProductParser.parseArgsForCall.last, expectedData)
        XCTAssertEqual(mockProductDataSourceDelegate.didFailToFetchDataCallCount, 1)
    }
    
    func testCollectionViewNumberOfItemsInSection_MatchesNumberOfProducts() {
        
        // Given

        let collectionView = MockCollectionView()
        let expectedData = "some_json_data".data(using: String.Encoding.utf8)
        mockURLSession.dataTaskReturns = (expectedData, nil, nil)
        mockProductParser.parseReturns = [ sampleProduct ]
        sut.fetchData()
        
        // When

        let count = sut.collectionView(collectionView, numberOfItemsInSection: 0)
        
        // Then

        XCTAssertEqual(count, 1)
    }
    
    func testCollectionViewCellForItemAt_ConfiguresCell() {
        
         // Given
        
        let collectionView = MockCollectionView()
        let expectedData = "some_json_data".data(using: String.Encoding.utf8)
        mockURLSession.dataTaskReturns = (expectedData, nil, nil)
        mockProductParser.parseReturns = [ sampleProduct ]
        
        // When
    
        sut.fetchData()
        guard let cell = sut.collectionView(collectionView, cellForItemAt: IndexPath(item: 0, section: 0)) as? MockProductCell else {
            XCTFail()
            return
        }
        
        // Then

        XCTAssertEqual(cell.configureArgsForCall.last, sampleProduct)
    }
    
}
