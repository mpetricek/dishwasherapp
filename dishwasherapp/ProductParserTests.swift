//
//  ProductParserTests.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 21/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import XCTest
@testable import dishwasherapp


class ProductParserTests: XCTestCase {

    var sut: ProductParser!
    
    override func setUp() {
        super.setUp()
        sut = ProductParser()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testParse_WhenHavingValidData_ParsesTheModel() {
        
        guard let products = sut.parse(data: jsonDataFromFixture("products")) else {
            XCTFail("JSON should have been parsing")
            return
        }
        
        XCTAssertEqual(products.count, 2)
        XCTAssertEqual(products[0].id, 3215462)
        XCTAssertEqual(products[0].title, "Dishwasher 1")
        XCTAssertEqual(products[0].priceInPounds, 34900)
        XCTAssertEqual(products[0].imageURL, URL(string: "http://example.com/image1"))
        XCTAssertEqual(products[1].id, 1955287)
        XCTAssertEqual(products[1].title, "Dishwasher 2")
        XCTAssertEqual(products[1].priceInPounds, 35900)
        XCTAssertEqual(products[1].imageURL, URL(string: "http://example.com/image2"))
    }
    
    func testParse_WhenHavingInvalidJSON_ProduceNil() {
        
        
        XCTAssertNil(sut.parse(data: "invalid-json".data(using: String.Encoding.utf8)!))
    }
    
    func testParse_WhenHavingInvalid_ParsesTheModelWithoutTheElement() {
        
        guard let products = sut.parse(data: jsonDataFromFixture("products-with-invalid-element")) else {
            XCTFail("JSON should have been parsing")
            return
        }
        
        XCTAssertEqual(products.count, 1)
        XCTAssertEqual(products[0].id, 3215462)
    }
    
    private func jsonDataFromFixture(_ fixture: String) -> Data {
        
        guard let path = Bundle(for: type(of: self)).path(forResource: fixture, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
                XCTFail("Unable to find JSON fixture")
                return Data()
        }
        
        return data
    }
}
