//
//  ProductCell.swift
//  dishwasherapp
//
//  Created by Mirek Petricek on 21/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCell: UICollectionViewCell {
    
    static let reuseIdentifier = "ProductCell"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        imageView.image = nil
        descriptionLabel.text = nil
        priceLabel.text = nil
    }
    
    func configure(with product: Product) {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = "£"
        descriptionLabel.text = product.title
        priceLabel.text = formatter.string(from: NSNumber(floatLiteral: Double(product.priceInPounds / 100)))
        imageView.sd_setImage(with: product.imageURL, placeholderImage: UIImage(named: "dishwasher_placeholder")?.withRenderingMode(.alwaysTemplate))
    }
}
