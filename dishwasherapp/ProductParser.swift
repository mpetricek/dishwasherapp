//
//  ProductParser.swift
//  dishwasherapp
//
//  Created by Mirek Petricek on 21/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import Foundation

class ProductParser {
    
    
    func parse(data: Data) -> [Product]? {
        
        var products = [Product]()
        
        do {
            
            guard let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let productsJSONArray = json["products"] as? [[String: Any]] else {
                    return nil
                }
            
            for product in productsJSONArray where product["type"] as? String == "product" {
                
                if let productID = Int(product["productId"] as? String ?? ""),
                    let title = product["title"] as? String,
                    let priceArray = product["price"] as? [String: Any],
                    let priceNowString = priceArray["now"] as? String,
                    let priceNowDouble = Double(priceNowString),
                    let imageURLString = product["image"] as? String,
                    let imageURL = URL(string: "http:\(imageURLString)")
                {
                
                    products.append(Product(id: productID, title: title, priceInPounds: Int(priceNowDouble * 100), imageURL: imageURL))
                }
            }
            
            return products
            
        } catch {
            return nil
        }
    }
}
