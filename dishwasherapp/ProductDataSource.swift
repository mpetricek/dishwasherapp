//
//  ProductDataSource.swift
//  dishwasherapp
//
//  Created by Mirek Petricek on 20/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import UIKit

protocol ProductDataSourceDelegate: class {
    
    func didFetchData()
    func didFailToFetchData()
}

class ProductDataSource: NSObject {
    
    static let productionEndpointURL = "https://api.johnlewis.com/v1/products/search?q=dishwasher&key=Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb&pageSize=20"
    
    weak var delegate: ProductDataSourceDelegate?
    private var products: [Product]?
    private let urlSession: URLSession
    private let productParser: ProductParser
    
    required init(urlSession: URLSession = URLSession.shared, productParser: ProductParser = ProductParser()) {
        self.urlSession = urlSession
        self.productParser = productParser
        super.init()
    }
    
    private lazy var productAPIEndpoint: URL? = {
        
        if let overridenAPIURLString = ProcessInfo.processInfo.environment["APIURL"] {
            return URL(string: overridenAPIURLString)
        }
        
        return URL(string: ProductDataSource.productionEndpointURL)
    }()
    
    func fetchData() {
        
        guard let url = productAPIEndpoint else {
            assertionFailure("API configuration error")
            delegate?.didFailToFetchData()
            return
        }
        
        urlSession.dataTask(with: url) { [weak self] (data, _, error) in
            
            guard error == nil,
                let data = data,
                let products = self?.productParser.parse(data: data) else {
                
                DispatchQueue.main.async {
                    self?.delegate?.didFailToFetchData()
                }

                return
            }
            
            self?.products = products
            
            DispatchQueue.main.async {
                self?.delegate?.didFetchData()
            }
        }.resume()
    }
}

extension ProductDataSource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.reuseIdentifier, for: indexPath) as? ProductCell,
            let product = products?[indexPath.item] else {
            assertionFailure("Unable to dequeue a cell for indexPath")
            return UICollectionViewCell(frame: .zero)
        }
        
        cell.configure(with: product)

        return cell
    }
}
