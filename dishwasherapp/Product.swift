//
//  Product.swift
//  dishwasherapp
//
//  Created by Mirek Petricek on 20/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import Foundation

struct Product: Equatable {
    
    let id: Int
    let title: String
    let priceInPounds: Int
    let imageURL: URL
}

func ==(lhs: Product, rhs: Product) -> Bool {
    return lhs.id == rhs.id
}
