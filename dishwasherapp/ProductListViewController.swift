//
//  ProductListViewController.swift
//  dishwasherapp
//
//  Created by Mirek Petricek on 20/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var dataSource = ProductDataSource()
    
    override func viewDidLoad() {

        super.viewDidLoad()

        title = "Dishwashers"
        dataSource.delegate = self
        collectionView.dataSource = dataSource
        
        collectionView?.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: ProductCell.reuseIdentifier)
        
        dataSource.fetchData()
    }
}

extension ProductListViewController: ProductDataSourceDelegate {
    
    func didFetchData() {
        
        spinner.stopAnimating()
        collectionView.isHidden = false
        collectionView.reloadData()
        let count = dataSource.collectionView(collectionView, numberOfItemsInSection: 0)
        title = "Dishwashers (\(count))"
    }
    
    func didFailToFetchData() {
        
        spinner.stopAnimating()
        collectionView.isHidden = true
    }
}

