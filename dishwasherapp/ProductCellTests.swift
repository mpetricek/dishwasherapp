//
//  ProductCellTests.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 05/07/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import XCTest
@testable import dishwasherapp


class ProductCellTests: XCTestCase {
    
    var sut: ProductCell!
    
    var imageView = UIImageView(frame: .zero)
    var descriptionLabel = UILabel(frame: .zero)
    var priceLabel = UILabel(frame: .zero)
    
    override func setUp() {
        super.setUp()
        
        sut = ProductCell(frame: .zero)
        sut.imageView = imageView
        sut.descriptionLabel = descriptionLabel
        sut.priceLabel = priceLabel
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func testConfigureCell_populatesCellContent() {
        
        let product = Product(id: 100, title: "a-title", priceInPounds: 10000, imageURL: URL(string: "http://example.com/image1")!)
        
        sut.configure(with: product)
        
        XCTAssertEqual(sut.descriptionLabel.text, "a-title")
        XCTAssertEqual(sut.priceLabel.text, "£100.00")
    }
    
    func testPrepareForReuse_clearsTheCell() {
        
        sut.descriptionLabel.text = "something"
        sut.priceLabel.text = "432423"
        
        sut.prepareForReuse()
        
        XCTAssertNil(sut.imageView.image)
        XCTAssertNil(sut.descriptionLabel.text)
        XCTAssertNil(sut.priceLabel.text)
    }

}

