//
//  MockAPIServer.swift
//  dishwasherappUITests
//
//  Created by Mirek Petricek on 20/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import Foundation
import GCDWebServers

class MockAPIServer {
    
    private let server: GCDWebServer
    
    init?() {
        
        server = GCDWebServer()
        
        try! server.start(options: [
            GCDWebServerOption_BindToLocalhost: true,
            GCDWebServerOption_Port: 8888,
            GCDWebServerOption_AutomaticallySuspendInBackground: false
            ])
    }
    
    deinit {
        server.stop()
    }
    
    func respondToProductRequest() {
        
        server.addHandler(forMethod: "GET", path: "/gridendpoint", request: GCDWebServerRequest.self, processBlock: { _ in
            
            return GCDWebServerDataResponse(data: self.productJSONResponse(), contentType: "application/json")
        })
    }
    
    private func productJSONResponse() -> Data {
        
        guard let path = Bundle(for: type(of: self)).path(forResource: "products", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
                fatalError("unable to find json fixture")
        }
        
        return data
    }
}
