//
//  MockProductParser.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 21/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import Foundation
@testable import dishwasherapp

class MockProductParser: ProductParser {
    
    var parseReturns: [Product]?
    var parseArgsForCall = [Data]()
    
    override func parse(data: Data) -> [Product]? {

        parseArgsForCall.append(data)
        return parseReturns
    }
}
