//
//  MockURLSession.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 21/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import Foundation

class MockURLSession: URLSession {
    
    typealias URLSessionReturnTuple = ((Data?, URLResponse?, Error?))
    
    var dataTaskReturns: URLSessionReturnTuple?
    
    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        
        return MockURLSessionDataTask(returnValues: dataTaskReturns, completionCallback: completionHandler)
    }
    
    class MockURLSessionDataTask: URLSessionDataTask {
        
        var dataTaskReturns: URLSessionReturnTuple?
        var dataTaskCompletion: ((Data?, URLResponse?, Error?) -> Void)?
        
        init(returnValues: URLSessionReturnTuple?, completionCallback: ((Data?, URLResponse?, Error?) -> Void)?) {
            dataTaskReturns = returnValues
            dataTaskCompletion = completionCallback
        }
        
        override func resume() {
            
            if let returnTuple = dataTaskReturns {
                dataTaskCompletion?(returnTuple.0, returnTuple.1, returnTuple.2)
            }
        }
    }
}
