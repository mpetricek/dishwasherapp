//
//  MockProductCell.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 06/07/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import UIKit
@testable import dishwasherapp

class MockProductCell: ProductCell {
    
    var configureArgsForCall = [Product]()
    override func configure(with product: Product) {
        configureArgsForCall.append(product)
    }
}

class MockCollectionView: UICollectionView {
    
    convenience init() {
        self.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    var reloadDataCount = 0
    
    override func reloadData() {
        reloadDataCount += 1
    }
    
    override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
        return MockProductCell(frame: .zero)
    }
}
