//
//  Extensions.swift
//  dishwasherappTests
//
//  Created by Mirek Petricek on 21/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func appear() {
        
        beginAppearanceTransition(true, animated: false)
        endAppearanceTransition()
    }
}
