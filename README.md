# dishwasherapp

## What is this?

dishwasherapp is a single iOS project to demonstrate my preferred way of writing code - Test Driven Development.

Functionwise it is a simple one-view app that reaches to the John Lewis API endpoint and presents a range of dishwasher products in a simple grid interface. The app is designed to be extensible.

iOS elements featured:

 - Basic MVC architecture
 - A simple UI storyboard interface design with UI autolayout
 - Use of `UICollectionView` and seperating the view from the data source via `UICollectionViewDataSource` protocol
 - Delegation pattern to decouple the data source from the View Controller
 - Using non-mutable models and protection of private data/methods.
 - Networking via `URLSession` API
 - JSON parsing using standard `JSONSerialisation` methods
 - Dynamic UICollectionView cell loading, instantiation from Nib
 - Non-blocking/cancellable image caching via SDWebImage
 - Unit testing using XCTest
 - UI Testing using XCUITest
 - Mocking internal classes by subclassing and dependency injection
 - Stubbing of external API endpoint by using on-board mock web server.


## Dependencies

dishwasherapp needs the latest version of Xcode (9.4.1) to run.

dishwasherapp uses  the following 3rd party dependencies:

- `SDWebImage` (https://github.com/rs/SDWebImage) to simplify dynamic downloads of images. Without this it would be necessary to handle image downloads and especially cancellation of pending downloads manually.
- `GCDWebServer` (https://github.com/swisspol/GCDWebServer) used only in the UI test target to implement API mock server for integration testing purposes.


Both dependencies are installed using `carthage` package manager. The system was chosen instead of the more common `cocoapods` because of its less invasive nature and simplicity. `carthage` can be installed using macbbrew or similar system:

`brew install carthage`

In order to simplify exploring the app, the pre-built frameworks binaries are included in the source repository. These would work as soon as the same version of Xcode is used (we all hope for a moment when Swift ABI will become stable). In other cases, dependencies need to be fetched/rebuilt using `carthage` dependency manager. Run the following from the top-level of the repository:

`carthage update`

This should donwload latest versions of the frameworks and compile them so that the Xcode can find them. 

## Project strategy when developing features

1. write/modify a failing UI test
2. write a failing unit test
3. implement the code change
4. make sure unit test is passing
5. make sure UI test is passing
6. refactor
7. done

## High-level architecture

- `ProductListViewController` - the main screen of the app. It is embeded inside a `UINavigationController` and features a `UICollectionView` view
- `ProductDataSource` - managed by the `ProductListViewController` to abstract fetching data from the network and maintaining a data model. It also implements `UICollectionViewDataSource` protocol to abstract the data model for the `UICollectionView`
- `Product` - data model representing one product element as provided by the API
- `ProductParser` - used by `ProductDataSource` to translate raw JSON data received from the network into an array of Product models.
- `ProductCell` - view class (UICollectionViewCell) representing a product cell on screen. 


## Test design

dishwasherapp contains three targets:

- `dishwasherapp` - the main app
- `dishwasherappTests` - unit tests
- `dishwasherappUITests` - UI/intergation tests

Test targets are integrated in the Xcode in a standard way.

Each app class is accompanied with a test class. The test classes purpose is encoded in the function name. Bodies of tests follow "given/when/then" convention to encourage writing behavioral tests.

The UI test contains a single test to excercise API/app integration. In order this to run the mock server, the TCP port 8888 must be available.
