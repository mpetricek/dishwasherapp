//
//  dishwasherappUITests.swift
//  dishwasherappUITests
//
//  Created by Mirek Petricek on 20/06/2018.
//  Copyright © 2018 Mirek Petricek. All rights reserved.
//

import XCTest

class DishwasherappUITests: XCTestCase {
    
    var app: XCUIApplication!
    var apiServer: MockAPIServer!
        
    override func setUp() {

        super.setUp()
        
        app = XCUIApplication()
        continueAfterFailure = false
        apiServer = MockAPIServer()
        app.launchEnvironment["APIURL"] = "http://localhost:8888/gridendpoint"
    }
    
    override func tearDown() {

        app = nil
        apiServer = nil
        super.tearDown()
    }
    
    func testItFetchesDataFromAPI_andDisplaysProducts() {
       
        // given
        apiServer.respondToProductRequest()
        app.launch()

        // when
        _ = app.wait(for: .runningForeground, timeout: 10)
        
        // then
        XCTAssertTrue(waitFor(app.navigationBars["Dishwashers (2)"], to: .appear))
        XCTAssertTrue(waitFor(app.staticTexts["Dishwasher 1"], to: .appear))
        XCTAssertTrue(waitFor(app.staticTexts["£349.00"], to: .appear))
    }
}

extension XCTestCase {
    
    enum Condition: String {
        case appear = "exists == true"
        case disappear = "exists == false"
    }
    
    @discardableResult
    func waitFor(_ element: XCUIElement, to condition: Condition) -> Bool {
        let predicate = NSPredicate(format: condition.rawValue)
        let expectationConstant = expectation(for: predicate, evaluatedWith: element, handler: nil)
        
        let result = XCTWaiter().wait(for: [expectationConstant], timeout: 5)
        return result == .completed
    }
}
